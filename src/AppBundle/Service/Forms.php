<?php

namespace AppBundle\Service;

use Symfony\Component\Form\Form;

/**
 * Class Form
 *
 * @package AppBundle\Service
 */
class Forms
{
	/**
	 * @param Form   $form
	 * @param string $format
	 *
	 * @return array
	 */
	public function getErrors(Form $form, $format = 'array')
	{
		$errors = [];

		foreach ($form as $fieldName => $formField) {
			foreach ($formField->getErrors(true) as $error) {
				$errors[$fieldName] = $error->getMessage();
			}
		}

		if ('string' === $format) {
			$errorsString = "";
			foreach ($errors as $error) {
				$errorsString .= $error.'<br />';
			}
			$errors = $errorsString;
		}

		return $errors;
	}
}
