<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * PostalCodeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PostalCodeRepository extends EntityRepository
{
	/**
	 * @param string $postalCode
	 *
	 * @return array
	 */
	public function beginWith($postalCode)
	{
		$qb = $this->createQueryBuilder('pc')
			->select('pc.postalCode')
			->where('pc.postalCode LIKE :postalCode')
			->setParameter(':postalCode', $postalCode."%");

		return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
	}
}
