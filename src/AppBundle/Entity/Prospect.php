<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Prospect
 *
 * @ORM\Table(name="prospect")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProspectRepository")
 */
class Prospect
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "Veuillez sélectionnez une civilité."
     * )
     *
     * @ORM\Column(name="gender", type="string", length=255)
     */
    private $gender;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "Le nom est obligatoire."
     * )
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "Le prénom est obligatoire."
     * )
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "Le code postal est obligatoire."
     * )
     *
     * @ORM\Column(name="postalCode", type="string", length=255)
     */
    private $postalCode;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "L'email est obligatoire."
     * )
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas valide."
     * )
     *
     * @ORM\Column(name="mail", type="string", length=255)
     */
    private $mail;

    /**
     * @var string
     * @Assert\NotBlank(
     *     message = "Le téléphone est obligatoire."
     * )
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var bool
     * @Assert\NotBlank(
     *     message = "Veuillez sélectionner une réponse pour recevoir ou non l'actualité Nexity."
     * )
     *
     * @ORM\Column(name="actuality", type="boolean")
     */
    private $actuality;

    /**
     * @var bool
     * @Assert\NotBlank(
     *     message = "Veuillez sélectionner une réponse pour recevoir ou non les offres partenaires."
     * )
     *
     * @ORM\Column(name="offer", type="boolean")
     */
    private $offer;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updatedAt", type="datetime")
	 */
    private $createdAt;


	/**
	 * Prospect constructor.
	 */
    public function __construct()
    {
    	$this->createdAt = new \DateTime();
    }

	/**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Prospect
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Prospect
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Prospect
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return Prospect
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Prospect
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Prospect
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set actuality
     *
     * @param boolean $actuality
     *
     * @return Prospect
     */
    public function setActuality($actuality)
    {
        $this->actuality = $actuality;

        return $this;
    }

    /**
     * Get actuality
     *
     * @return bool
     */
    public function getActuality()
    {
        return $this->actuality;
    }

    /**
     * Set offer
     *
     * @param boolean $offer
     *
     * @return Prospect
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return bool
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Prospect
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
