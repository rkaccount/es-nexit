<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProspectType
 *
 * @package AppBundle\Form
 */
class ProspectType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('gender')
			->add('name')
			->add('firstName')
			->add('postalCode')
			->add('mail')
			->add('phone')
			->add('actuality', IntegerType::class)
			->add('offer', IntegerType::class);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class'      => 'AppBundle\Entity\Prospect',
			'csrf_protection' => false,
		]);
	}
}
