<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Prospect;
use AppBundle\Form\Type\ProspectType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ApiController
 *
 * @Route("api")
 * @package AppBundle\Controller
 */
class ApiController extends Controller
{
	/**
	 * @param Request $request
	 *
	 * @Route("/form", name="submit_form")
	 * @Method({"POST"})
	 *
	 *
	 * @return JsonResponse
	 */
	public function submitFormAction(Request $request)
	{
		$data     = $request->request->all();
		$prospect = new Prospect();

		$form = $this->createForm(ProspectType::class, $prospect);
		$form->submit($data);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($prospect);
			$em->flush();

			return new JsonResponse([
				"message" => "Message de validation du formulaire",
			], 201);
		}

		return new JsonResponse([
			"errors" => $this->get('app.form')->getErrors($form, 'string'),
		], 400);
	}

	/**
	 * @param Request $request
	 *
	 * @Route("/autocomplete/postalCode", name="autocomplete_postal_code")
	 * @Method({"GET"})
	 *
	 *
	 * @return JsonResponse
	 */
	public function autocompletePostalCodeAction(Request $request)
	{
		$postalCodes = $this->getDoctrine()->getRepository("AppBundle:PostalCode")->beginWith($request->get('postal_code'));

		return new JsonResponse($postalCodes);
	}
}
