$ ->
  
  bg = $("#cover")
  pin = $("#cover .pin img")
  
  xMouseNew = 0
  xDeltaNew = 0
  
  $(window).on "mousemove", (evt) -> 
  
    if evt.clientX != xMouseNew
      
      larg = $(window).width()
      if larg > 768
        xMouseNew = evt.clientX
        xDeltaNew = (xMouseNew * 2 / larg - 1)
      else
        xMouseNew = 0
        xDeltaNew = 0
        
      posBG = ((xDeltaNew + 1) * 10 + 50) + "% 0%, center"
      posPin = xDeltaNew * 5 + "px"
      bg.css("background-position", posBG)
      pin.css("transform", "translateX(" + posPin + ")")

  $('form').on "submit", (evt) ->
    evt.preventDefault()
    $.ajax({
      type:     "POST"
      dataType:  "json"
      url:      $(this).attr('action')
      data:     $(this).serialize()
    })
    .done (response) ->
      $('form').hide()
      $("#success .message").html(response.message);
      $("#success").show()
    .fail (jqXHR, textStatus) ->
      if (jqXHR.responseJSON.hasOwnProperty('errors'))
        $('#error').html(jqXHR.responseJSON.errors)
      else
        $('#error').html("Une erreur s'est produite lors de l'envoi du formulaire, veuillez réessayer.");

  options = {
    url: (parameter) ->
      return "http://nexity.local/app_dev.php/api/autocomplete/postalCode?postal_code="+parameter
    getValue: 'postalCode'
    requestDelay: 200
  };

  $('#postalCode').easyAutocomplete(options)