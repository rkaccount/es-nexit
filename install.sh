#!/bin/bash

echo "Composer Install"
composer install

echo "------------------"
echo "Création de la base de donnée"
bin/console doctrine:schema:update --force

echo "------------------"
echo "Chargement des données des codes postaux"
bin/console doctrine:fixtures:load

echo "------------------"
echo "Génération de l'intégration"
cd web/nexity
npm install
gulp

echo "------------------"
echo "Votre projet est prêt à être utilisé"
exit 0
