es-nexity
=========

Ce projet regroupe une intégration d'un formulaire d'inscription et un webservice permettant d'enregistrer les 
données du formulaire et d'autocompléter le champ "Code postal".
La décision a été prise de ne pas intégrer l'intégration (jade/coffee/saas) dans Symfony par manque de temps et 
d'informations de la part de l'équipe. Cela peut être une évolution du projet.

## Installation

1/ Créer la base de donnée

2/ Lancer la commande `sh install.sh` à la racine du projet, et laissez vous guider. 
Celle ci va vous installer le projet **Symfony** et builder l'intégration du formulaire.

3/ L'url de test est : **http://nexity.local/app_dev.php/**. Par manque de temps, l'url n'est pas passée en paramètre 
lors de la build. Il faudra donc la modifier dans 2 fichiers : 
- **web/nexity/src/jade/index.jade** l44
- **web/nexity/src/coffee/form.coffee** l46

Le point 3/ pourrait faire parti d'un évolution proche.

4/ Pour vérifier que l'API fonctionne bien, lancez les tests avec la commande `phpunit`.

Une évolution pourrait-être d'intégrer un CasperJs pour tester directement le formulaire. Mais en l'état, l'API est tout 
de même testée à un niveau satisfaisant.

## Code Postaux

Les codes postaux sont intégrés par des fixtures. Cela se fait via la commande `sh install.sh` du début de projet ou via 
la commande `bin/console doctrine:fixtures:load`.
Les données sont issues du site **data.gouv.fr**.

## Équipe du projet

- Intégration : Damien Doussaud (Namide)
- Développement WS : Raphaël Kueny (raphael@kueny.fr)
- Chef de projet : ---
