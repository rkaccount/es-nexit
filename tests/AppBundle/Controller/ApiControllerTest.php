<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApiControllerTest
 *
 * @package Tests\AppBundle\Controller
 */
class ApiControllerTest extends WebTestCase
{
    public function testSubmitForm()
    {
        $client = static::createClient();

	    $client->request('GET', '/api/form');
	    $this->assertEquals(405, $client->getResponse()->getStatusCode());

        $client->request('POST', '/api/form');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());

	    $data = [
		    'gender'     => 'Mister',
		    'name'       => 'Doe',
		    'firstName'  => 'John',
		    'postalCode' => '31450',
		    'mail'       => 'johndoe@gmail.com',
		    'phone'      => '0600000000',
		    'actuality'  => 1,
		    'offer'      => 0,
	    ];
	    $client->request('POST', '/api/form', $data);
	    $this->assertEquals(201, $client->getResponse()->getStatusCode());

	    $data = [
		    'gender'     => 'Mister',
		    'name'       => 'Doe',
		    'firstName'  => 'John',
		    'postalCode' => '31450',
		    'mail'       => 'johndoe',
		    'phone'      => '0600000000',
		    'actuality'  => 1,
	    ];
	    $client->request('POST', '/api/form', $data);
	    $this->assertEquals(400, $client->getResponse()->getStatusCode());
	    $response = json_decode($client->getResponse()->getContent());
	    $this->assertEquals(
	    	"L'email '\"johndoe\"' n'est pas valide.<br />Veuillez sélectionner une réponse pour recevoir ou non les offres partenaires.<br />",
		    $response->errors
	    );
    }

    public function testautocompletePostalCode()
    {
	    $client = static::createClient();

	    $client->request('POST', '/api/autocomplete/postalCode');
	    $this->assertEquals(405, $client->getResponse()->getStatusCode());

	    $client->request('GET', '/api/autocomplete/postalCode?postal_code=3145');
	    $this->assertEquals(200, $client->getResponse()->getStatusCode());
	    $response = json_decode($client->getResponse()->getContent());
	    $this->assertEquals("31450", $response[0]->postalCode);
    }
}
